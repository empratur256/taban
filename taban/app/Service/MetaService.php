<?php

namespace App\Service;

use App\Models\Product;

class MetaService
{
    public function __construct()
    {

    }

    public function get(Product $product, $key)
    {
        return $product->metas()->where("key", $key)->firstOrFail()->value;
    }

    public function exists(Product $product, $key)
    {
        return $product->metas()->where("key", $key)->exists();
    }

    public function update(Product $product, $key, $value)
    {
        $product->metas()->where("key", $key)->update([
            "key" => $key,
            "value" => $value
        ]);
    }

    public function create(Product $product, $key, $value)
    {
        $product->metas()->create([
            "key" => $key,
            "value" => $value
        ]);
    }
}
