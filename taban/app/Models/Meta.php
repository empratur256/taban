<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function prodact()
    {
        return $this->belongsTo(Product::class);
    }
}
