<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $with = [
        "metas",
        "contents"
    ];

    public function contents()
    {
        return $this->morphMany(Content::class, "content");
    }

    public function metas()
    {
        return $this->hasMany(Meta::class);
    }

    public function toArray()
    {
        foreach ($this->metas as $item) {
                $data[$item->key] = $item->value;
        }
        $data["id"] = $this->id;
        $data["image"] = $this->contents()->get();
        return $data;
    }
}
