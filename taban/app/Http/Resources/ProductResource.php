<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this['id'],
            "images" => ContentResource::collection($this['image']),
            "title" => $this['title'],
            "description" => $this['description'],
            "amount" => $this['amount'],
            "price" => $this['price'],
            "counter" => $this['counter'] ?? 0,
        ];
    }
}
