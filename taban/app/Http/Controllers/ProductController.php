<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Repo\Product\ProductRepoInterface;


class ProductController extends Controller
{
    protected $productRepo;

    public function __construct(ProductRepoInterface $productRepo)
    {
        $this->productRepo = $productRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ProductRepoInterface $productRepo
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ProductResource::collection(collect($this->productRepo->index()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return ProductResource
     */
    public function store(ProductRequest $request)
    {
        return new ProductResource($this->productRepo->store($request));
    }

    /**
     * Display the specified resource.
     *
     * @param Product $id
     * @return ProductResource
     */
    public function show(Product $product)
    {
        return new ProductResource($this->productRepo->show($product));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductRequest $request
     * @param Product $product
     * @return ProductResource
     */
    public function update(ProductRequest $request, Product $product)
    {
        return new ProductResource($this->productRepo->update($request, $product));
    }

    /**
     * Remove the specified resource from storage.
     * @param Product $product
     * @return bool|null
     */
    public function destroy(Product $product)
    {
        return $this->productRepo->destroy($product);
    }

    /**
     * @param Product $product
     * @return mixed
     */
    public function buy(Product $product)
    {
        return $this->productRepo->buy($product);
    }
}
