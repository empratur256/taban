<?php

namespace App\Repo\Product;

use App\Models\Product;
use Illuminate\Http\Request;

interface ProductRepoInterface
{
    public function index():array;
    public function store(Request $request);
    public function show(Product $product);
    public function update(Request $request, Product $product);
    public function destroy(Product $product);
    public function buy(Product $product);
}
