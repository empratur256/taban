<?php

namespace App\Repo\Product;

use App\Jobs\BuyJob;
use App\Models\Product;
use App\Service\MetaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductRepo implements ProductRepoInterface
{
    protected $product;
    protected $metaService;

    public function __construct(Product $product, MetaService $metaService)
    {
        $this->product = $product;
        $this->metaService = $metaService;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(): array
    {
        return $this->product->with(["contents", "metas"])->get()->toArray();
    }

    public function store(Request $request)
    {
        $product = $this->product->create([
            "type" => "product"
        ]);
        $this->images($product, $request->allFiles());

        $this->meta($product, $request->toArray());

        return $product->toArray();
    }

    public function update(Request $request, Product $product)
    {
        $this->images($product, $request->allFiles());

        $this->meta($product, $request->toArray(), "update");

        return $product->first()->toArray();
    }

    public function show(Product $product)
    {
        return $product->toArray();
    }

    public function destroy(Product $product)
    {
        foreach ($product->contents as $item) {
            Storage::delete($item->path);
            $item->delete();
        }
        $product->metas()->delete();
        return $product->delete();
    }

    public function buy(Product $product)
    {
        dispatch(new BuyJob($product));
        return true;
    }

    private function images(Product $product, $arr = [])
    {
        foreach ($arr as $value) {
            $name = $value[0]->getClientOriginalName();
            $path = $product->id . "/" . $name;
            if ($this->checkFile($value[0]->path(), $product)) {
                Storage::put($path, file_get_contents($value[0]));
                $product->contents()->create([
                    "name" => $name,
                    "path" => $path
                ]);
            }
        }
    }

    private function checkFile($file, Product $product): bool
    {
        foreach ($product->contents()->get() as $item) {
            $files = Storage::path($item->path);
            if (md5_file($files) == md5_file($file)) {
                return false;
            }
        }
        return true;
    }

    private function meta(Product $product, $arr = [], $create = "create")
    {
        foreach ($arr as $key => $value) {
            if (!is_array($value)) {
                $this->metaService->{$create}($product, $key, $value);
            }
        }
    }
}
