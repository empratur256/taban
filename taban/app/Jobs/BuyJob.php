<?php

namespace App\Jobs;

use App\Models\Product;
use App\Service\MetaService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BuyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $product;
    protected $metaService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetaService $metaService)
    {
        $this->metaService = $metaService;
        $counter = $this->metaService->exists($this->product, "counter");
        if ($counter) {
            $counter = (int)$this->metaService->get($this->product, "counter");
            $this->metaService->update($this->product, "counter", ++$counter);
        } else {
            $this->metaService->create($this->product, "counter", 1);
        }
        $amount = (int)$this->metaService->get($this->product, "amount");
        $this->metaService->update($this->product, "amount", --$amount);
    }
}
