<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ProductTest extends TestCase
{
    public function test_index()
    {
        $response = $this->get(route("products.index"));
        $response->assertStatus(200);
    }

    public function test_store()
    {
        Storage::fake('avatars');
        $file = UploadedFile::fake()->image("avatars.jpeg");
        $data = [
            "title" => "test",
            "description" => "test",
            "amount" => "1000",
            "price" => "1000",
            "image" => [$file],
        ];

        $response = $this->post(route("products.store"), $data);
        $response->assertStatus(200);
    }

    public function test_show()
    {
        $product = Product::first();
        $response = $this->get(route("products.show", ["product" => $product->id]));
        $response->assertStatus(200);
    }

    public function test_update()
    {
        $product = Product::first();
        Storage::fake('avatars');
        $file = UploadedFile::fake()->image("avatars.jpeg");
        $data = [
            "title" => "test",
            "description" => "test",
            "amount" => "1000",
            "price" => "1000",
            "image" => [$file],
        ];

        $response = $this->put(route("products.update", ["product" => $product->id]), $data);
        $response->assertStatus(200);
    }

    public function test_destroy()
    {
        $product = Product::first();
        $response = $this->delete(route("products.destroy", ["product" => $product->id]));
        $response->assertStatus(200);
    }

    public function test_buy()
    {
        $product = Product::first();
        $response = $this->post(route("products.buy", ["product" => $product->id]));
        $response->assertStatus(200);
    }
}
